<?php
 session_start();
?>
<html>
<head>
 <title>Storing an array with a session</title>
</head>
<body>
<h1>Product Choice Page</h1>
<?php
if (isset($_POST[form_products])) {
if (!empty($_SESSION[products])) {
$products = array_unique(
array_merge(unserialize($_SESSION[products]),
$_POST[form_products]));
 }
 $_SESSION[products] = serialize($products);
 print "<p>Your products have been registered!</p>";
 }
?>
<form method="POST" action="<?php $_SERVER[PHP_SELF] ?>">
<P>Select some products:<br>
<select name="form_products[]" multiple size=3>
<option>Sonic Screwdriver</option>
<option>Hal 2000</option>
<option>Tardis</option>
<option>ORAC</option>
<option>Transporter bracelet</option>
</select>
 <br><br>
<input type="submit" value="choose">
</form>
<br><br>
<a href="acessing.php">content page</a>
</body>
</html>
